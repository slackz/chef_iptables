actions :allow_input, :execute, :stateful_ruleset

attribute :command,   :kind_of => String
attribute :chain,     :regex => /INPUT|OUTPUT|FORWARD/, :default => 'INPUT'
attribute :interface, :kind_of => String
attribute :protocol,  :kind_of => String
attribute :source,    :kind_of => String # CIDR... eg: '127.0.0.0/8'
attribute :dest,      :kind_of => String 
attribute :port,      :regex => /(\d)+/
