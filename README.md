chef_iptables Cookbook
======================
A simple cookbook to manage iptables rules through chef. 

Goals
------
iptables is an excellent tool with great documentation.  This project does not interfere with the flexibility and syntax of the iptables command; it merely offers various hooks to help manage rules in your chef cookbooks.

Requirements
------------
chef + iptables :)

Usage
-----
```ruby
chef_iptables 'basic stateful setup' do 
  action :stateful_ruleset
end

chef_iptables 'foo' do 
  command '-L -n'
  action :execute
end

chef_iptables 'allow statsd' do 
  action :allow_input
  port 8125
  protocol 'udp'
  interface node['network']['internal_nic']
end
```

License  
---------------
This code is under the MIT/X11 license.  See LICENSE for details

